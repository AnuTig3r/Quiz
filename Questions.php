<?php
/**
 * Questions
 *
 * @package RosarioSIS
 * @subpackage modules
 */

require_once 'modules/Quiz/includes/common.fnc.php';
require_once 'modules/Quiz/includes/Questions.fnc.php';
require_once 'modules/Quiz/includes/Quizzes.fnc.php';

DrawHeader( ProgramTitle() );

$_REQUEST['category_id'] = empty( $_REQUEST['category_id'] ) ? '' : $_REQUEST['category_id'];
$_REQUEST['id'] = empty( $_REQUEST['id'] ) ? '' : $_REQUEST['id'];

QuizQuestionAllowEdit( $_REQUEST['id'] );

if ( AllowEdit()
	&& isset( $_POST['tables'] )
	&& is_array( $_POST['tables'] ) )
{
	$table = isset( $_REQUEST['table'] ) ? $_REQUEST['table'] : null;

	require_once 'ProgramFunctions/MarkDownHTML.fnc.php';

	foreach ( (array) $_REQUEST['tables'] as $id => $columns )
	{
		if ( isset( $columns['DESCRIPTION'] ) )
		{
			$columns['DESCRIPTION'] = SanitizeHTML( $_POST['tables'][ $id ]['DESCRIPTION'] );
		}

		// FJ fix SQL bug invalid sort order.
		if ( empty( $columns['SORT_ORDER'] )
			|| is_numeric( $columns['SORT_ORDER'] ) )
		{
			// FJ added SQL constraint TITLE is not null.
			if ( ! isset( $columns['TITLE'] )
				|| ! empty( $columns['TITLE'] ) )
			{
				// Trim Answer.
				if ( ! empty( $columns['ANSWER'] ) )
				{
					// Remove trailing white spaces and new lines from answer / options.
					$columns['ANSWER'] = trim( $columns['ANSWER'] );
				}

				// Update Question / Category.
				if ( $id !== 'new' )
				{
					if ( isset( $columns['CATEGORY_ID'] )
						&& $columns['CATEGORY_ID'] != $_REQUEST['category_id'] )
					{
						$_REQUEST['category_id'] = $columns['CATEGORY_ID'];
					}

					$sql = 'UPDATE ' . $table . ' SET ';

					foreach ( (array) $columns as $column => $value )
					{
						$sql .= DBEscapeIdentifier( $column ) . "='" . $value . "',";
					}

					$sql = mb_substr( $sql, 0, -1 ) . " WHERE ID='" . $id . "'";

					$go = true;
				}
				// New Question / Category.
				else
				{
					$sql = 'INSERT INTO ' . $table . ' ';

					// New Question.
					if ( $table === 'QUIZ_QUESTIONS' )
					{
						if ( isset( $columns['CATEGORY_ID'] ) )
						{
							$_REQUEST['category_id'] = $columns['CATEGORY_ID'];

							unset( $columns['CATEGORY_ID'] );
						}

						// @deprecated use DBSeqNextID() since 4.5.
						$id_RET = DBGet( DBQuery( 'SELECT ' . db_seq_nextval( 'QUIZ_QUESTIONS_ID_SEQ' ) . ' AS ID ' ) );

						$_REQUEST['id'] = $id_RET[1]['ID'];

						$fields = 'ID,CATEGORY_ID,CREATED_BY,';

						$values = "'" . $_REQUEST['id'] . "','" . $_REQUEST['category_id'] . "','" . User( 'STAFF_ID' ) . "',";
					}
					// New Category.
					elseif ( $table === 'QUIZ_CATEGORIES' )
					{
						// @deprecated use DBSeqNextID() since 4.5.
						$id_RET = DBGet( DBQuery( 'SELECT ' . db_seq_nextval( 'QUIZ_CATEGORIES_ID_SEQ' ) . ' AS ID ' ) );

						$_REQUEST['category_id'] = $id_RET[1]['ID'];

						$fields = "ID,";

						$values = "'" . $_REQUEST['category_id'] . "',";
					}

					// School, Created by.
					$fields .= 'SCHOOL_ID,';

					$values .= "'" . UserSchool() . "',";

					$go = false;

					foreach ( (array) $columns as $column => $value )
					{
						if ( ! empty( $value )
							|| $value == '0' )
						{
							$fields .= DBEscapeIdentifier( $column ) . ',';

							$values .= "'" . $value . "',";

							$go = true;
						}
					}

					$sql .= '(' . mb_substr( $fields, 0, -1 ) . ') values(' . mb_substr( $values, 0, -1 ) . ')';
				}

				if ( $go )
				{
					DBQuery( $sql );
				}
			}
			else
				$error[] = _( 'Please fill in the required fields' );
		}
		else
			$error[] = _( 'Please enter valid Numeric data.' );
	}

	// Unset tables & redirect URL.
	RedirectURL( array( 'tables' ) );
}

// Delete Question / Category.
if ( $_REQUEST['modfunc'] === 'delete'
	&& AllowEdit() )
{
	if ( intval( $_REQUEST['id'] ) > 0 )
	{
		if ( DeletePrompt( _( 'Question' ) ) )
		{
			DBQuery( "DELETE FROM QUIZ_QUESTIONS
				WHERE ID='" . $_REQUEST['id'] . "'
				AND SCHOOL_ID='" . UserSchool() . "'" );

			// Unset modfunc & ID & redirect URL.
			RedirectURL( array( 'modfunc', 'id' ) );
		}
	}
	elseif ( isset( $_REQUEST['category_id'] )
		&& intval( $_REQUEST['category_id'] ) > 0
		&& ! QuizCategoryHasQuestions( $_REQUEST['category_id'] ) )
	{
		if ( DeletePrompt( dgettext( 'Quiz', 'Question Category' ) ) )
		{
			DBQuery( "DELETE FROM QUIZ_CATEGORIES
				WHERE ID='" . $_REQUEST['category_id'] . "'
				AND SCHOOL_ID='" . UserSchool() . "'" );

			// Unset modfunc & category ID redirect URL.
			RedirectURL( array( 'modfunc', 'category_id' ) );
		}
	}
}

if ( ! $_REQUEST['modfunc'] )
{
	echo ErrorMessage( $error );

	$RET = array();

	// ADDING & EDITING FORM.
	if ( ! empty( $_REQUEST['id'] )
		&& $_REQUEST['id'] !== 'new' )
	{
		$RET = DBGet( DBQuery( "SELECT ID,CATEGORY_ID,TITLE,TYPE,
			DESCRIPTION,SORT_ORDER,ANSWER,FILE,CREATED_AT,CREATED_BY,
			(SELECT TITLE
				FROM QUIZ_CATEGORIES
				WHERE ID=CATEGORY_ID) AS CATEGORY_TITLE
			FROM QUIZ_QUESTIONS
			WHERE ID='" . $_REQUEST['id'] . "'
			AND SCHOOL_ID='" . UserSchool() . "'" ) );

		$RET = $RET[1];

		$title = $RET['CATEGORY_TITLE'] . ' - ' . $RET['TITLE'];

		// Set Question Category ID if not set yet.
		if ( empty( $_REQUEST['category_id'] ) )
		{
			$_REQUEST['category_id'] =  $RET['CATEGORY_ID'];
		}
	}
	elseif ( ! empty( $_REQUEST['category_id'] )
		&& $_REQUEST['category_id'] !== 'new'
		&& empty( $_REQUEST['id'] ) )
	{
		$RET = DBGet( DBQuery( "SELECT ID AS CATEGORY_ID,TITLE,SORT_ORDER
			FROM QUIZ_CATEGORIES
			WHERE ID='" . $_REQUEST['category_id'] . "'" ) );

		$RET = $RET[1];

		$title = $RET['TITLE'];
	}
	elseif ( ! empty( $_REQUEST['id'] )
		&& $_REQUEST['id'] === 'new' )
	{
		$title = dgettext( 'Quiz', 'New Question' );

		$RET['ID'] = 'new';

		$RET['CATEGORY_ID'] = isset( $_REQUEST['category_id'] ) ? $_REQUEST['category_id'] : null;
	}
	elseif ( $_REQUEST['category_id'] === 'new' )
	{
		$title = dgettext( 'Quiz',  'New Question Category' );

		$RET['CATEGORY_ID'] = 'new';
	}

	echo QuizGetQuestionsForm(
		$title,
		$RET,
		isset( $extra_fields ) ? $extra_fields : array()
	);

	echo QuizGetQuestionAuthorHeader( $RET );

	echo QuizGetAddQuestionToQuizForm( $RET );

	// CATEGORIES.
	$categories_RET = DBGet( DBQuery( "SELECT ID,TITLE,SORT_ORDER
		FROM QUIZ_CATEGORIES
		WHERE SCHOOL_ID='" . UserSchool() . "'
		ORDER BY SORT_ORDER,TITLE" ) );

	// DISPLAY THE MENU.
	echo '<div class="st">';

	QuizQuestionsMenuOutput( $categories_RET, $_REQUEST['category_id'] );

	echo '</div>';

	// QUESTIONS.
	if ( ! empty( $_REQUEST['category_id'] )
		&& $_REQUEST['category_id'] !=='new'
		&& $categories_RET )
	{
		$questions_RET = DBGet( DBQuery( "SELECT ID,TITLE,TYPE,SORT_ORDER
			FROM QUIZ_QUESTIONS
			WHERE CATEGORY_ID='" . $_REQUEST['category_id'] . "'
			AND SCHOOL_ID='" . UserSchool() . "'
			ORDER BY SORT_ORDER,TITLE" ),
		array(
			'TYPE' => 'QuizMakeQuestionType',
			'TITLE' => 'QuizTruncateTitle',
		) );

		echo '<div class="st">';

		QuizQuestionsMenuOutput( $questions_RET, $_REQUEST['id'], $_REQUEST['category_id'] );

		echo '</div>';
	}
}
