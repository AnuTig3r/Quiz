Quiz Module
===========

![screenshot](https://gitlab.com/francoisjacquet/Quiz/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/quiz-module/

Version 1.4 - March, 2019

License GNU GPL v2

Author François Jacquet

DESCRIPTION
-----------
Quiz module for RosarioSIS. Teachers can add questions and answers to the questions database, and then add them their quizzes.
Quizzes are open for submission based on the corresponding Assignment's assigned and due dates. Teachers can decide whether to show correct answers to students and display questions in random order.
Students take their tests / quizzes online. Select, multiple, gap fill and text question types can be graded automatically.
Inline help.


CONTENT
-------
Quiz
- Quizzes
- Questions

INSTALL
-------
Copy the `Quiz/` folder (if named `Quiz-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 4.2+
