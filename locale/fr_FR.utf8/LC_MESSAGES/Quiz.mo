��             +         �  E   �               (     /     8     @  '   P     x  
   �  '   �     �     �     �  =   �          '     ;  	   M     W     d     t     |     �     �  %   �     �     �  ,      !   -  (   O    x  I   �  
   �     �     �     �  	          +         L     [  3   i     �      �     �  >   �     	     %	     >	  	   V	  	   `	     j	     y	     ~	     �	      �	  /   �	     
     
  ,   '
     T
  1   t
                                  
                                                       	                                                       *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answered Answers Back to Quizzes Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz Quizzes Quiz submitted. Quizzes Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Submit Quiz Text with gaps The sky is __blue__.
The grass is __green__. You alreay have graded this Quiz. You are not allowed to access this quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-23 15:13+0100
PO-Revision-Date: 2018-11-23 15:36+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2;ndgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 *Choix correct #1
Choix incorrect #2
Choix incorrect #3
*Choix correct #4 *Vrai
Faux Ajouter la Question Réponse Répondu Réponses Retour aux Quiz Délimiter les trous par deux tirets bas __ Texte à Trous Noter le Quiz Marquer les réponse correctes d'une astérisque *. Nouvelle Question Nouvelle Catégorie de Questions Nouveau Quiz Aucun Devoir disponible. Merci d'ajouter un nouveau Devoir: %s Question Catégories de Questions Catégorie de Questions Questions Quiz Quiz Quiz répondu. Quiz Ordre des Questions Aléatoire Options à Choix Simple Afficher les Réponses Correctes Il n'est plus possible de répondre à ce quiz. Soumettre le Quiz Texte à trous Le ciel est __bleu__.
L'herbe est __verte__. Vous avez déjà noté ce Quiz. Vous n'êtes pas autorisé a accéder à ce quiz. 