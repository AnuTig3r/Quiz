��             +         �  E   �               (     /     8     @  '   P     x  
   �  '   �     �     �     �  =   �          '     ;  	   M     W     d     t     |     �     �  %   �     �     �  ,      !   -  (   O  0  x  T   �     �       	   $  
   .  
   9     D  /   Y     �     �  3   �     �     �     	  @   	     ^	     g	     	  	   �	     �	     �	     �	     �	     �	      
  (   $
     M
     `
  ,   r
  "   �
  /   �
                                  
                                                       	                                                       *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answered Answers Back to Quizzes Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz Quizzes Quiz submitted. Quizzes Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Submit Quiz Text with gaps The sky is __blue__.
The grass is __green__. You alreay have graded this Quiz. You are not allowed to access this quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-23 15:16+0100
PO-Revision-Date: 2018-11-23 15:16+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2;ngettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 *Opción correcta #1
Opción incorrecta #2
Opción incorrecta#3
*Opción correcta #4 *Correcto
Incorrecto Agregar Pregunta Respuesta Respondida Respuestas Volver a las Pruebas Delimita los blancos con doble guiones bajos __ Llene los Blancos Calificar la Prueba Marcar las respuestas correctas con un asterisco *. Nueva Pregunta Nueva Categoría de Preguntas Nueva Prueba No hay Tareas disponibles. Por favor agregue una nueva Tarea: %s Pregunta Categoría de Preguntas Categoría de Preguntas Preguntas Prueba Pruebas Prueba entregada Pruebas Orden de Preguntas Aleatorio Selección de Opción Simple Mostrar las Respuestas Correctas No es más posible entregar esta prueba. Entregar la Prueba Texto con blancos El cielo es __azul__.
El pasto es __verde__. Esta Prueba ya ha sido calificada. No está autorizado para acceder a esta prueba. 