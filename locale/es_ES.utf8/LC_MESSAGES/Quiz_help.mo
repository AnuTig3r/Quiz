��          �      �       H    I  X   X  S   �  U        [  �   n  �     ;   �  �   �  W   ~  d   �  B   ;  h   ~  	  �     �  h   	  e   w	  a   �	     ?
  �   T
  �     9   �  �   �  [   �  z   �  L   j     �     	              
                                              "Select One from Options" fields create radio buttons from which you can select one option. To create this type of field, click on "Select One from Options" and then add your options (one per line) in the "Options" text box. Mark the correct answer with an asterisk "*". "Text" fields create alphanumeric text fields with a maximum capacity of 255 characters. <i>Questions</i> lets you add, edit, and browse Questions, organized in Categories. <i>Quizzes</i> lets you consult quizzes created by teachers, for the current quarter. Add a new Question Click on the "+" icon below the "No Questions were found" text. Fill in the Question field(s), and then choose what type of field you wish with the "Type" pull-down. First, select a Quiz by clicking on its title in the Quizzes list. The Quiz information and options are now displayed above the list. Just below the Quiz information, you will notice two links: Questions belonging to the selected Quiz are shown in the list next to the Quizzes list.
		Clicking on a Question title will bring you to the <i>Questions</i> program. The "Preview" link to display the Quiz and its Questions, just as students will see it. The "Sort Order" determines the order in which the fields will be displayed in the Student Info tab. The "Teacher" link will bring you to the <i>User Info</i> program. To consult Quizzes of another quarter, select the right Quarter from the dropdown list in the left menu. Project-Id-Version: Quiz module Help
POT-Creation-Date: 2018-11-23 15:16+0100
PO-Revision-Date: 2018-11-23 15:16+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _help
X-Poedit-SearchPath-0: Help_en.php
 Selección de Opción Simple Los campos de "Texto" crean un campo de texto alfanumérico con una capacidad máxima de 255 caracteres. <i>Preguntas</i> le permite agregar, modificar y consultar las Preguntas, organizadas en Categorías. <i>Pruebas</i> le permite consultar pruebas creadas por los docentes, para el bimestre corriente. Agregar una Pregunta Haga clic en el icono "+" bajo el texto "No se encontró ningún(a) Pregunta". LLene el campo Pregunta, y luego seleccione el tipo de pregunta deseado con el menú desplegable "Tipo". Primero, seleccione una Prueba haciendo clic en su tituló en la lista de Pruebas. Los datos de la Prueba y sus opciones están ahora a la vista arriba de la lista. Debajo de los datos de la Prueba se pueden ver 2 enlaces: Questions belonging to the selected Quiz are shown in the list next to the Quizzes list.
		Clicking on a Question title will bring you to the <i>Questions</i> program. El enlace "Prevista" para la Prueba y sus Preguntas, justo como los estudiantes las verán. La "Orden" determina la orden en la cual los campos son mostrados en la pestaña del programa Información del Estudiante. El enlace "Docente" lo apuntara al programa <i>Información del Usuario</i>. Para consultar las Pruebas de otro bimestre, selecciones el Bimestre deseado desde el menú desplegable del menú de izquierda. 