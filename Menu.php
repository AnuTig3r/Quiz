<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the Quiz module
 * - Add Menu entries to other modules
 *
 * @package Quiz module
 */

/**
 * Use dgettext() function instead of _() for Module specific strings translation
 * see locale/README file for more information.
 */
$module_name = dgettext( 'Quiz', 'Quiz' );

// Menu entries for the Quiz module.
$menu['Quiz']['admin'] = array( // Admin menu.
	'title' => dgettext( 'Quiz', 'Quiz' ),
	'default' => 'Quiz/Quizzes.php', // Program loaded by default when menu opened.
	'Quiz/Quizzes.php' => dgettext( 'Quiz', 'Quizzes' ),
	'Quiz/Questions.php' => dgettext( 'Quiz', 'Questions' ),
);

$menu['Quiz']['teacher'] = array( // Teacher menu.
	'title' => dgettext( 'Quiz', 'Quiz' ),
	'default' => 'Quiz/Quizzes.php', // Program loaded by default when menu opened.
	'Quiz/Quizzes.php' => dgettext( 'Quiz', 'Quizzes' ),
	'Quiz/Questions.php' => dgettext( 'Quiz', 'Questions' ),
);

$menu['Quiz']['parent'] = array( // Parent & student menu.
	'title' => dgettext( 'Quiz', 'Quiz' ),
	'default' => 'Quiz/StudentQuizzes.php', // Program loaded by default when menu opened.
	'Quiz/StudentQuizzes.php' => dgettext( 'Quiz', 'Quizzes' ),
);
