<?php
/**
 * Quiz common functions
 *
 * @package Quiz module
 */


/**
 * Get Quiz details from DB.
 *
 * @example $quiz = GetQuiz( $quiz_id, UserStudentID() );
 *
 * @param string $quiz_id Quiz ID.
 * @param integer $student_id Student ID, defaults to 0.
 *
 * @return boolean|array Quiz details array or false.
 */
function GetQuiz( $quiz_id, $student_id = 0 )
{
	global $_ROSARIO;

	if ( ! isset( $_ROSARIO['quiz'] ) )
	{
		$_ROSARIO['quiz'] = array();
	}

	if ( ! empty( $_ROSARIO['quiz'][ $quiz_id ] ) )
	{
		return $_ROSARIO['quiz'][ $quiz_id ];
	}

	// Check Quiz ID is int > 0.
	if ( ! $quiz_id
		|| (string) (int) $quiz_id !== $quiz_id
		|| $quiz_id < 1 )
	{
		return false;
	}

	$assignment_file_sql = version_compare( ROSARIO_VERSION, '4.4-beta', '<' ) ?
		",NULL AS ASSIGNMENT_FILE" :
		",ga.FILE AS ASSIGNMENT_FILE";

	$quiz_sql = "SELECT q.ID,q.STAFF_ID,
		q.TITLE,q.OPTIONS,ga.ASSIGNED_DATE,ga.DUE_DATE,
		(SELECT SUM(qq.POINTS) FROM QUIZ_QUIZXQUESTION qq WHERE qq.QUIZ_ID=q.ID) AS POINTS,
		(SELECT 1
			FROM QUIZ_ANSWERS qa,QUIZ_QUIZXQUESTION qq2 WHERE
			qq2.QUIZ_ID=q.ID
			AND qq2.ID=qa.QUIZXQUESTION_ID " .
			( $student_id > 0 ? "AND qa.STUDENT_ID=ss.STUDENT_ID " : '' ) .
			" LIMIT 1) AS ANSWERED,
		q.DESCRIPTION,c.TITLE AS COURSE_TITLE,
		q.CREATED_AT,
		q.CREATED_BY,
		gat.COLOR AS ASSIGNMENT_TYPE_COLOR,
		ga.TITLE AS ASSIGNMENT_TITLE" . $assignment_file_sql .
		" FROM GRADEBOOK_ASSIGNMENTS ga,COURSES c,GRADEBOOK_ASSIGNMENT_TYPES gat,QUIZ q";

	if ( $student_id > 0 )
	{
		$quiz_sql .= ",SCHEDULE ss";
	}

	$quiz_sql .= " WHERE q.ID='" . $quiz_id . "'
		AND q.SCHOOL_ID='" . UserSchool() . "'
		AND ga.ASSIGNMENT_ID=q.ASSIGNMENT_ID
		AND ga.MARKING_PERIOD_ID='" . UserMP() . "'
		AND gat.ASSIGNMENT_TYPE_ID=ga.ASSIGNMENT_TYPE_ID";

	if ( $student_id > 0 )
	{
		$quiz_sql .= " AND ss.STUDENT_ID='" . $student_id . "'
		AND ss.SYEAR='" . UserSyear() . "'
		AND ss.SCHOOL_ID='" . UserSchool() . "'
		AND ss.MARKING_PERIOD_ID IN (" . GetAllMP( 'QTR', UserMP() ) . ")
		AND (ga.COURSE_PERIOD_ID IS NULL OR ss.COURSE_PERIOD_ID=ga.COURSE_PERIOD_ID)
		AND (ga.COURSE_ID IS NULL OR ss.COURSE_ID=ga.COURSE_ID)
		AND (ga.ASSIGNED_DATE IS NULL OR CURRENT_DATE>=ga.ASSIGNED_DATE)
		AND (ga.DUE_DATE IS NULL
			OR (ga.DUE_DATE>=ss.START_DATE
				AND (ss.END_DATE IS NULL OR ga.DUE_DATE<=ss.END_DATE)))
		AND c.COURSE_ID=ss.COURSE_ID"; // Why not?
		// @todo Remove Due date checks, and let QuizCanSubmit handle it??
	}

	$quiz_RET = DBGet( DBQuery( $quiz_sql ), array(), array( 'ID' ) );

	$_ROSARIO['quiz'][ $quiz_id ] = isset( $quiz_RET[ $quiz_id ] ) ?
		$quiz_RET[ $quiz_id ][1] : false;

	return $_ROSARIO['quiz'][ $quiz_id ];
}

/**
 * Truncate Title to 36 chars
 * for responsive display in List.
 * Full title is in tooltip.
 *
 * @see Can be called through DBGet()'s functions parameter
 *
 * @param  string $value  Title value.
 * @param  string $column 'TITLE' (optional). Defaults to ''.
 */
function QuizTruncateTitle( $value, $column = '' )
{
	// Truncate value to 36 chars.
	$title = mb_strlen( $value ) <= 36 ?
		$value :
		'<span title="' . $value . '">' . mb_substr( $value, 0, 33 ) . '...</span>';

	return $title;
}


function QuizGetQuestions( $quiz_id, $functions = array(), $index = array() )
{
	$functions = ! empty( $functions ) && is_array( $functions ) ? $functions : array();
	$index = ! empty( $index ) && is_array( $index ) ? $index : array();

	$questions_RET = DBGet( DBQuery( "SELECT qqq.ID,qqq.QUESTION_ID,qqq.QUIZ_ID,qq.TITLE,qqq.POINTS,
		qq.TYPE,qq.DESCRIPTION,qq.ANSWER,qq.FILE,qqq.SORT_ORDER
		FROM QUIZ_QUIZXQUESTION qqq,QUIZ_QUESTIONS qq
		WHERE qq.SCHOOL_ID='" . UserSchool() . "'
		AND qqq.QUIZ_ID='" . $quiz_id . "'
		AND qq.ID=qqq.QUESTION_ID
		ORDER BY qqq.SORT_ORDER,qq.TITLE" ), $functions, $index );

	return $questions_RET;
}


/**
 * Quiz Get Option
 * If no option is specified, get all options.
 *
 * @param array  $quiz
 * @param string $option
 *
 * @return array|string Array of options or option.
 */
function QuizGetOption( $quiz, $option = '' )
{
	static $options_cache = array();

	if ( empty( $quiz['OPTIONS'] ) )
	{
		return ( $option === '' ? array() : '' );
	}
	elseif ( isset( $options_cache[ $quiz['ID'] ] ) )
	{
		$options = $options_cache[ $quiz['ID'] ];
	}
	else
	{
		$options = unserialize( $quiz['OPTIONS'] );

		$options_cache[ $quiz['ID'] ] = $options;
	}

	if ( $option === '' )
	{
		return $options;
	}

	return ( ! isset( $options[ $option ] ) ? '' : $options[ $option ] );
}



function QuizHasAnswers( $quiz_id, $student_id = 0 )
{
	if ( $quiz_id < 1 ||
		$student_id < 0 )
	{
		return false;
	}

	$quiz_answered_RET = DBGet( DBQuery( "SELECT 1
		FROM QUIZ_ANSWERS qa,QUIZ_QUIZXQUESTION qq
		WHERE qq.QUIZ_ID='" . (int) $quiz_id . "'
		AND qq.ID=qa.QUIZXQUESTION_ID " .
		( $student_id > 1 ? "AND qa.STUDENT_ID='" . (int) $student_id . "'" : '' ) .
		" LIMIT 1" ) );

	return $quiz_answered_RET;
}



function QuizIsGraded( $quiz_id, $student_id = 0 )
{
	if ( $quiz_id < 1 ||
		$student_id < 0 )
	{
		return false;
	}

	$quiz_graded_RET = DBGet( DBQuery( "SELECT 1
		WHERE EXISTS(SELECT 1
			FROM QUIZ_ANSWERS qa,QUIZ_QUIZXQUESTION qq
			WHERE qq.QUIZ_ID='" . (int) $quiz_id . "'
			AND qq.ID=qa.QUIZXQUESTION_ID " .
			( $student_id > 0 ? "AND qa.STUDENT_ID='" . (int) $student_id . "'" : '' ) .
			" AND qa.POINTS IS NOT NULL
			LIMIT 1)" ) );

	return (bool) $quiz_graded_RET;
}


// MultipleCheckboxInput()was added in 4.2, here is a replacement.
if ( ! function_exists( 'MultipleCheckboxInput' ) )
{
	/**
	 * Multiple Checkbox Input
	 * Do not forget to add '[]' (array) after your input name.
	 *
	 * @since 4.2
	 *
	 * @example CheckboxInput( $value, 'values[' . $id . '][' . $name . '][]' );
	 *
	 * @uses GetInputID() to generate ID from name
	 * @uses InputDivOnclick()
	 *       if ( AllowEdit() && !isset( $_REQUEST['_ROSARIO_PDF'] ) && ! $new && $div )
	 * @uses InputDivOnclick()
	 *
	 * @param  string  $value   Input value(s), delimited by 2 pipes. For example: '||Value1||Value2||'.
	 * @param  string  $name    Input name.
	 * @param  string  $title   Input title (optional). Defaults to ''.
	 * @param  array   $options Input options: array( option_value => option_text ).
	 * @param  string  $extra   Extra HTML attributes added to the input. (optional).
	 * @param  boolean $div     Is input wrapped into <div onclick>? (optional). Defaults to true.
	 *
	 * @return string  Inputs HTML
	 */
	function MultipleCheckboxInput( $value, $name, $title, $options, $extra = '', $div = true )
	{
		$id = GetInputID( $name );

		$required = $value == '' && mb_strpos( $extra, 'required' ) !== false;

		$ftitle = FormatInputTitle( $title, $id, $required );

		$multiple_value = ( $value != '' ) ?
			str_replace( '||', ', ', mb_substr( $value, 2, -2 ) ) :
			'-';

		if ( ! AllowEdit()
		 	|| isset( $_REQUEST['_ROSARIO_PDF'] ) )
		{
			return $multiple_value . $ftitle;
		}

		$multiple_html = '<table class="cellpadding-5"><tr class="st">';

		$i = 0;

		foreach ( (array) $options as $option )
		{
			if ( $i++ % 3 == 0 )
			{
				$multiple_html .= '</tr><tr class="st">';
			}

			$multiple_html .= '<td><label>
				<input type="checkbox" name="' . $name . '"
					value="' . htmlspecialchars( $option, ENT_QUOTES ) . '" ' . $extra . ' ' .
					( $option != '' && mb_strpos( $value, $option ) !== false ? ' checked' : '' ) . ' />&nbsp;' .
				( $option != '' ? $option : '-' ) .
			'</label></td>';
		}

		$multiple_html .= '</tr></table>';

		$multiple_html .= str_replace( '<br />' , '', $ftitle );

		if ( $value != ''
			&& $div )
		{
			$return = InputDivOnclick(
				$id,
				$multiple_html,
				$multiple_value,
				$ftitle
			);
		}
		else
			$return = $multiple_html;

		return $return;
	}
	}
